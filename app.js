import regeneratorRuntime from './lib/runtime/runtime.js';
// 引入封装好的请求数据的js文件
import { request } from './request/index.js';
//app.js
const QQMapWX = require("./assets/libs/qqmap-wx-jssdk.min");
let qqmapsdk = new QQMapWX({
  key: "EIYBZ-OR6C4-WYDU5-XSZ5M-N7IDT-FQFC6"
})


App({
  async onLaunch () {
    await this.initPage();
    await this.getCityList();
  },
  initPage(){
    //获取用户授权信息，防止重复出现授权弹框
    wx.getSetting({
      success: res => {
        //已有权限直接获得信息，否则出现授权弹框
        if (res.authSetting['scope.userLocation']) {
          this.getUserLocation()
        } else {
          this.getUserLocation()
        }
      },
    })
  },
  //获取用户的位置信息
  getUserLocation(){
    let _this = this;
    wx.getLocation({
      //授权成功
      success: (res) => {
        const latitude = res.latitude;
        const longitude = res.longitude;
        console.log(latitude,"-------------",longitude);
        //获取城市名称
        qqmapsdk.reverseGeocoder({
          location: {
            latitude,
            longitude
          },
          success: (res) => {
            console.log(res);
            const cityFullname = res.result.address_component.city;
            const cityInfo = {
              latitude,
              longitude,
              cityName: cityFullname.substring(0,cityFullname.length-1)
            }
            console.log(cityInfo);
            //由于getUserInfo是网络请求，可能会在Page.onLoad之后才返回，
            //所以此处加入callback以防止这种情况
            if(this.userLocationReadyCallback){
              this.userLocationReadyCallback()
            }
          },
          fail: () => {
            //防止弹框出现后，用户长时间不选择
            if(this.userLocationReadyCallback){
              this.userLocationReadyCallback();
            }
          }
        })

      },
      fail(){
        wx.showModal({
          title: '全流量时代-温馨提示',
          content: '请您重新进行授权位置信息',
          showCancel: true,
          cancelText: '取消',
          cancelColor: '#000000',
          confirmText: '确定',
          confirmColor: '#3CC51F',
          success: (result) => {
            if (result.confirm) {
              wx.openSetting({
                success: (result) => {
                  _this.initPage();
                  console.log(result);
                }
              });
            }
          }
          
        });
          
      }
    })
  },

  // 获取城市列表信息
  async getCityList(){
    let res = await request("/api/film/region/get");
    if(res.data.status == 0) {
      console.log(res.data.data);
    }
  },

  globalData: {
    userInfo: "幻化成风",
    cityInfo: {
      movie:{
        show_type: 0,
        city_code: 110100,
        latitude: 39.95933,
        longitude: 116.29845,
        page_index: 1,
        page_size: 20,
      },
      cinema: {
        city_id: 3,
        city_code: 110100,
        latitude: 39.95933,
        longitude: 116.29845,
        page_index: 1,
        page_size: 20
      }
      
    }
  }
})