// components/select-movie/select-movie.js
import regeneratorRuntime from '../../lib/runtime/runtime.js';
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    movies: {
      type: Array,
      value: []
    },
    movie: {
      type: Object
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    movies: [],
    movie: null,  //选中的电影
    scrollLeft: 0,  //设置滚动条的位置
    size: 0,    //电影item的大小
    i:0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //选中电影
    async selectMovie(e){
      const {movies} = this.data;
      const movie = (e && e.currentTarget.dataset.movie)||movies[this.data.i];
      if(movies.length && this.data.movie && movie.show_id === this.data.movie.show_id){
        return;
      }
      const index = movies.findIndex(item => item.show_id === movie.show_id);
      if(this.data.size) {
        this.setData({
          movie,
          scrollLeft: this.data.size * index
        })
      } else {
        await this.calcSize().then((size) => {
          this.setData({
            movie,
            size,
            scrollLeft: size * index
          })
        })
      }
      this.triggerEvent('selectMovie',{
        movie
      })
    },

    //计算节点大小
    calcSize() {
      return new Promise((resolve, reject) => {
        const query = wx.createSelectorQuery().in(this)
        query.select(`#item1`).fields({
          size: true,
          computedStyle: ['margin-left']
        }, function(res) {
          let size = 0
          if (res) {
            size = res.width + parseFloat(res['margin-left'])
          }
          resolve(size)
        }).exec()
      })
    }
  }
})
