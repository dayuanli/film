//封装请求数据接口的代码
let times = 0;
export const request = (apiUrl,params) => {
  //定义公共接口前缀
  const baseUrl = "https://g.qll-times.com";
  //每请求一次就加一次
  times++;
  wx.showLoading({
    title: "加载中"
  });
  //发送请求数据
  return new Promise((resolve,resject) => {
    wx.request({
      url: baseUrl+apiUrl,
      data: params,
      header: {
        "accept": "*/*",
        "content-type": "application/json"
      },
      success: (result) => {
        resolve(result)
      },
      fail: (err) => {
        resject(err);
      },
      //接口调用结束的回调函数（success, fail 都会调用）
      complete: () => {
        times--;
        if(times == 0) {
          wx.hideLoading();
          wx.stopPullDownRefresh();
        }
      }
    })
  })
    
}