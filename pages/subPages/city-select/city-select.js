// pages/tabBar/cinema/cinema.js
import regeneratorRuntime from '../../../lib/runtime/runtime.js';
// 引入封装好的请求数据的js文件
import { request } from '../../../request/index.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    cityNameList: [],
    searchCity: [],     //搜索出来的值
    showSearch: 0,      //
    cityNmae: "武汉"
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getCityList();
  },

  // 获取城市数据
  async getCityList(){
    let _this = this;
    let res = await request("/api/film/region/get");
    if(res.data.status == 0){
      _this.setData({
        cityNameList: res.data.data
      })
    }
  },

  search(e){
    const value = e.detail.value.trim().toUpperCase()
    let result = [];
    if(value) {
      this.setData({
        showSearch: 1
      })
      result = this.data.cityNameList.filter(item => {
        return item.pin_yin.toUpperCase().includes(value)||item.region_name.includes(value)
      })
    }else{
      this.setData({
        showSearch: 0
      })
    }
    this.setData({
      searchCity: result
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})