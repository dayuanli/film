// pages/subPages/movie-detail/movie-detail.js
// 支持es7的async语法
import regeneratorRuntime from '../../../lib/runtime/runtime.js';
// 引入封装好的请求数据的js文件
import { request } from '../../../request/index.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    movieDetail: {},
    movieStills: [],
    movieStills_2: [],
    images: [],
    still_hidden: true       //是否有数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let _this= this;
    let {movie} =JSON.parse(options.data);
    this.setData({
      movieDetail: _this.handleData(movie)
    })
    this.getMovieStills(this.data.movieDetail);
    
  },

  gobuy(e){
    let movie = JSON.stringify(e.currentTarget.dataset.movie);
    wx.navigateTo({
      url: "/pages/subPages/movie-buy/movie-buy?data="+movie
    })
  },


  //统一处理数据的接口
  handleData(data){
    if(!data){
      return false;
    }
    let obj = data;
    obj.sc  = this.formatStar(obj.remark/2);
    obj.leading_role = obj.leading_role || "暂无演员";
    obj.show_name_en = obj.show_name_en || obj.show_name;
    obj.director = obj.director || "暂无导演";
    obj.open_time = obj.open_time || "暂时没有数据"
    return obj;
  },

  formatStar(sc){
    let stars = new Array(5).fill('star-empty');
    const fullStars = Math.floor(sc); 
    const halfStar = sc % 1 ? 'star-half' : 'star-empty' //半星的个数，半星最多1个
    stars.fill('star-full', 0, fullStars) 
    if(fullStars < 5) {
      stars[fullStars] =halfStar;
    }
    return stars
  },

  //点击预览图片
  previewImage(e){
    const imgsrc = "https://gw.alicdn.com/tfscom/"+e.currentTarget.dataset.imgsrc
    wx.previewImage({
      current: imgsrc,
      urls: [imgsrc]
    })
  },
  //点击预览图片  ----> 剧照
  async previewImage2(e){
    const {index} = e.currentTarget.dataset;
    const urls = this.data.movieStills_2.map(item => 
     "https://gw.alicdn.com/tfscom/"+item.still_url
    )
    wx.previewImage({
      urls,
      current: urls[index]
    })
  },

  //图片展示不出来就使用默认图片
  binderrorimg: function(e){
    let index = e.target.dataset.index;    
    let strImg = "https://image.qll-times.com/2020/01/c2461e2943ff4c6480e2179434efa7c9.png";
    let temp = 'movieStills['+index+']';
    this.setData({
      [temp]: strImg
    })
  },

  async getMovieStills(data){
    let _this = this;
    wx.showLoading({
      title: "加载中"
    });
    let params = {
      show_id: data.t_id,
      show_type: data.show_type,
    };
    let res = await request("/api/film/stills/get",params);
    if(res.data.status == 0){
      let list2 = [];
      let list1 = [];
      if(res.data.data != ""){
        for(let i=0; i<(res.data.data.length>15?15:res.data.data.length);i++){
          await list2.push(res.data.data[i]);
          let item = "https://gw.alicdn.com/tfscom/"+res.data.data[i].still_url+"_480x480.jpg"
          await list1.push(item);
        }
        _this.setData({
          movieStills: list1,
          movieStills_2:list2,
          still_hidden: true
        })
      }else{
        _this.setData({
          movieStills: [],
          movieStills_2: [],
          still_hidden: false
        })
      }
    }else{
      wx.showToast({
        title: res.data.message,
        icon: 'none'
      })
    }
  },

  // 进行图片高度固定，宽度自适应
  async imageLoad(e){
    let $width = e.detail.width;
    let $height = e.detail.height;
    let ratio=$width/$height;

    let viewHeight = 300;
    let viewWidth = viewHeight * ratio;
    let image = await this.data.images;
    image[e.target.dataset.index] = {
      width: viewWidth,
      height: viewHeight
    }
    this.setData({
      images: image
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})