// pages/tabBar/cinema/cinema.js
import regeneratorRuntime from '../../../lib/runtime/runtime.js';
// 引入封装好的请求数据的js文件
import { request } from '../../../request/index.js';
let app =  getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    city: "北京",
    cinema: [],
    loading: 1,
    timeList: [],    //时间
    activeTime: 0,   //设置当前选中的时间
    noData:0
  },
  params: app.globalData.cityInfo.cinema,
  totalPage: null,      //总的电影院数量
  dayjs: require('../../../utils/day'),
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.data){
      console.log(JSON.parse(options.data));
      this.params.show_id = JSON.parse(options.data).t_id;
    }
    let today = this.dayjs();
    this.params.show_date = today.format("YYYY-MM-DD");
    //设置7天的格式
    let timeList = [];
    for(let i=0; i<7; i++){
      timeList.push({dayTime: "",week:""})
      timeList[i].dayTime = today.add(i,'day').format("MM-DD")
      timeList[i].week = this.week(today.add(i,'day').day());
    }
    timeList[0].week = '今天'
    timeList[1].week = '明天'
    timeList[2].week = '后天'
    this.setData({
      timeList
    })
    this.getcinemaList();   
  },
  
  async getcinemaList(){
    let _this = this;
    _this.setData({
      noData: 0
    })
    let res = await request("/api/film/cinema/page/get",this.params)
    if(res.data.status == 0) {
      _this.setData({
        cinema: [...this.data.cinema,...res.data.data.list]
      })
      _this.totalPage = JSON.parse(res.data.data.total_page);
      if(_this.data.cinema == ""){
        _this.setData({
          noData: 1
        })
      }
      if(this.totalPage<=1){
        _this.setData({
          loading: 0
        })
      }
    }else{
      wx.showToast({
        title: res.data.message,
        icon: "none"
      })
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      cinema: [],
      loading: 1
    })
    this.params.page_index = 1;
    wx.pageScrollTo({
      scrollTop: 0
    })
    this.getcinemaList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.params.page_index >= this.totalPage){
      this.setData({
        loading: 0
      })
      return wx.showToast({
        title: "加载完毕",
        icon: "none"
      })
    }
    this.params.page_index++;
    this.getcinemaList();
  },

  // 点击进入影院详情页面
  goCinemaDetail(e){
    e.currentTarget.dataset.cinema.show_id = this.params.show_id;
    let cinema =JSON.stringify(e.currentTarget.dataset.cinema);
    wx.navigateTo({
      url: '/pages/subPages/cinema-detail/cinema-detail?data='+cinema
    })
  },

  //到电影院中查看详情
  gocinema(e){

  },

  //顶部的日期选中
  selectTime(e){
    let {selectindex} = e.currentTarget.dataset;
    let today = this.dayjs().add(selectindex,'day').format("YYYY-MM-DD");
    this.params.show_date = today
    this.setData({
      activeTime: selectindex,
      cinema: []
    })
    this.getcinemaList();
  },

  //日期 对应的 周几
  week(data){
    switch(data){
      case 0:
        return "周日"
        break;
      case 1:
          return "周一"
          break;
      case 2:
          return "周二"
          break;
      case 3:
          return "周三"
          break;
      case 4:
          return "周四"
          break;
      case 5:
        return "周五"
        break;
      case 6:
        return "周六"
        break;
    }
  }  
})