// pages/subPages/cinema-detail/cinema-detail.js
// pages/tabBar/cinema/cinema.js
import regeneratorRuntime from '../../../lib/runtime/runtime.js';
// 引入封装好的请求数据的js文件
import { request } from '../../../request/index.js';
let app =  getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: { 
    cinema_id: "",
    cinema_name: "",
    cinema_address: "",
    movie: null,  //选中的电影
    movies: [], //电影列表
    city_id: null,    //城市id
    city_code: null,  //城市编码

    selectDay: "",
    dayData: [],      //排片日期
    Scheduling: {},   //影片排期数据
  },
  params: {
    // "id": 2914095,
    // "t_id": 6219,
    // "city_id": 3,
    // "city_code": 110100,
    // "schedule_close_time": 15,
    // "latitude": "39.958346",
    // "longitude": "116.291503",
    // "region_name": "海淀区",
    // "cinema_name": "英嘉星美影城（原星美金源IMAX店）",
    // "address": "海淀区世纪金源购物中心5层东首",
    // "phone": "010-88859095 ",
    // "standard_id": "11061601",
    // "support_third_party_refund": "0",
    // "sync_flag": 0,
    // "created_at": "2020-01-03 02:00:11",
    // "updated_at": "2020-01-03 02:00:16",
    // "juli": 603,
    // "price": "41.34"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad (options) {
    this.params = JSON.parse(options.data);
    this.setData({
      cinema_id: this.params.t_id,
      city_code: this.params.city_code,
      city_id: this.params.city_id,
      cinema_name: this.params.cinema_name,
      cinema_address: this.params.address
    })
    if(this.params.show_id){
      this.setData({
        show_id: this.params.show_id
      })
    }
    await this.getMovieList();
    if(this.data.movie){
      this.getMovieDay(this.data.movie);
    }
  },

  //获取指定影院的电影列表
  async getMovieList(){
    let _this = this;
    let params = {
      cinema_id: _this.data.cinema_id
    };
    let res = await request("/api/film/cinema/shows/get",params);
    if(res.data.status == 0) {
      _this.setData({
        movies: res.data.data.data,
        movie: res.data.data.data[0]||null
      })
      if(_this.params.show_id){
        for(let i=0; i<this.data.movies.length;i++){
          if(this.data.movies[i].show_id == this.data.show_id){
            let array = this.data.movies.splice(i,1);
            let array2 = [...array,...this.data.movies]
            _this.setData({
              movies: array2,
              movie: array2[0]
            })
          }
        }
      }
    }else{
      wx.showToast({
        title: res.data.message,
        icon: 'none',
      });
    }
  },

  selectMovie(e){
    let movie;
    if(e){
      movie = e.detail.movie;
    }else{
      movie = this.data.movie;
    }
    this.setData({
      movie: movie
    })
    this.getMovieDay(this.data.movie);
  },

  //获取影片排期
  async getMovieDay(data){
    let _this = this;
    let params = {
      city_id: this.data.city_id,
      city_code: this.data.city_code,
      show_id: data.show_id,
      cinema_id: data.cinema_id
    }
    let res = await request("/api/film/cinema/schedule/get",params);
    if(res.data.status == 0) {
      _this.setData({
        dayData: _this.handleData(res.data.data.data),
        Scheduling: _this.handleScheduling(res.data.data.data[0]),
        selectDay: res.data.data.data[0].show_date
      })
    }else{
      return wx.showToast({
        title: res.data.message,
        icon: "none"
      })
    }
  },

  //day处理获取到的数据格式化
  handleData(data){
    if(!data) return false;
    for(let i=0; i<data.length; i++){
      if(data[i].show_date.length > 6){
        data[i].day_show = data[i].show_date.substr(0,2);
        data[i].day_rili = data[i].show_date.substr(2);
      }else{
        data[i].day_show = "预售";
        data[i].day_rili = data[i].show_date;
      }
    }
    return data;
  },

  //处理排期数据格式化
  handleScheduling(data){
    let _data = data.return_value[0].schedules
    if(_data){
      for(let i=0;i<_data.length;i++){
        _data[i].start_time = _data[i].show_time.split(" ")[1].substr(0,5);
        _data[i].close_times = _data[i].close_time.split(" ")[1].substr(0,5);
      }
      return _data
    }
  },

  async selectDay(e){
    let _this = this;
    this.setData({
      Scheduling: [],
      selectDay: e.currentTarget.dataset.item.show_date
    })
    let Scheduling = e.currentTarget.dataset.item||0;
    if(Scheduling){
      this.setData({
        Scheduling: await _this.handleScheduling(Scheduling)
      })
    }
  },

  buy(){
    wx.navigateTo({
      url: '/pages/subPages/development/development'
    });
      
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})