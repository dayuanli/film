const app = getApp();

// 支持es7的async语法
import regeneratorRuntime from '../../../lib/runtime/runtime.js';
// 引入封装好的请求数据的js文件
import { request } from '../../../request/index.js';

// pages/tabBar/movie/movie.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hotFilm: [],   //正在热映 的数据
    city: "北京",    //默认城市
    count: null,     //电影数量
    switchItem: 0,

    loading: 1,           //页面加载
  },

  params: app.globalData.cityInfo.movie,
  totalPage: null,     //总页面数量

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getHotFilmList();
  },

  async getHotFilmList() {
    wx.showLoading({
      title: "加载中"
    });
    let _this = this;
    let res = await request("/api/film/shows/get",_this.params);
    this.totalPage = Math.ceil(res.data.data.count / _this.params.page_size);
    if (res.data.status == 0) {
      _this.setData({
        hotFilm: [..._this.data.hotFilm,...res.data.data.rows],
        count: res.data.data.count
      })
    }
  },

  async selectItem(e){
    wx.pageScrollTo({
      scrollTop: 0
    })
    const item = e.currentTarget.dataset.item;
    this.params.page_index = 1;
    this.totalPage = 0;
    this.setData({
      hotFilm: [],
      switchItem: item,
      loading: 1
    })
    if(item === 1) {
      this.params.show_type = 1;
    }else{
      this.params.show_type = 0;
    }
    await this.getHotFilmList();
  },

  Jump(e){
    let movie = JSON.stringify(e.currentTarget.dataset);
    wx.navigateTo({
      url: '/pages/subPages/movie-detail/movie-detail?data='+movie,
    });
  },

  //点击去购买
  goBuy(e){
    let movie = JSON.stringify(e.currentTarget.dataset.movie);
    wx.navigateTo({
      url: "/pages/subPages/movie-buy/movie-buy?data="+movie
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      hotFilm: []
    })
    this.params.page_index = 1;
    this.getHotFilmList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.params.page_index >= this.totalPage){
      this.setData({
        loading: 0
      })
      return wx.showToast({
        title: "数据已加载完毕",
        icon: "none"
      })
    }
    this.params.page_index++;
    this.getHotFilmList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})