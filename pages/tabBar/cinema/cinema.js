// pages/tabBar/cinema/cinema.js
import regeneratorRuntime from '../../../lib/runtime/runtime.js';
// 引入封装好的请求数据的js文件
import { request } from '../../../request/index.js';
let app =  getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    city: "北京",
    cinema: [],
    loading: 1
  },
  params: app.globalData.cityInfo.cinema,
  totalPage: null,      //总的电影院数量
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getcinemaList();
  },
  
  async getcinemaList(){
    let _this = this;
    let res = await request("/api/film/cinemas/get",this.params)
    if(res.data.status == 0) {
      _this.setData({
        cinema: [...this.data.cinema,...res.data.data.list]
      })
      this.totalPage = JSON.parse(res.data.data.total_page);
    }else{
      wx.showToast({
        title: res.data.message,
        icon: "none"
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      cinema: [],
      loading: 1
    })
    this.params.page_index = 1;
    wx.pageScrollTo({
      scrollTop: 0
    })
    this.getcinemaList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log(this.params.page_index);
    if(this.params.page_index >= this.totalPage){
      this.setData({
        loading: 0
      })
      return wx.showToast({
        title: "加载完毕",
        icon: "none"
      })
    }
    this.params.page_index++;
    this.getcinemaList();
  },

  // 点击进入影院详情页面
  goCinemaDetail(e){
    let cinema =JSON.stringify(e.currentTarget.dataset.cinema);
    wx.navigateTo({
      url: '/pages/subPages/cinema-detail/cinema-detail?data='+cinema
    })
  },
  
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})